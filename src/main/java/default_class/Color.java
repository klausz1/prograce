package default_class;

public class Color {
	private int red;
	private int green;
	private int blue;

	public Color(int r, int g, int b) {
		// TODO : constarints
		this.green = g;
		this.red = r;
		this.blue = b;
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public String getColorCode() {
		return this.red + "," + this.blue + "," + this.green;
	}
}
