package default_class;

public class Coordinate {
	private int x;
	private int y;
	private int z;
	
	
	
	public Coordinate(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getZ() {
		return z;
	}
	public void setZ(int z) {
		this.z = z;
	}
	
	public int[] getValue() {
		int[] tomb = null;
		tomb[0] = x;
		tomb[1] = y;
		tomb[2] = z;
		return tomb;
	}
	public int[] toArray() {
		int[] array= null;
		array[0] = x;
		array[1] = y;
		array[2] = z;
		return array;
	}
	public void setValues(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		
	}
}
