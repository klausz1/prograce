package default_class;

public class Point {
	private Color color;
	private Coordinate coordinat;
	
	public Point(int x, int y, int z, Color color) {
		int r = color.getRed();
		int b = color.getBlue();
		int g = color.getGreen();
		this.color = new Color(r, g, b);
		this.coordinat = new Coordinate(x, y, z);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public String getColorCode() {
		return this.color.getColorCode();
	}

	public int[] getCoordinate() {
		return this.coordinat.toArray();
	}

	public void setCoordinat(int x, int y, int z) {
		this.coordinat.setValues(x, y, z);
	}
	
	
}
