package default_class;

import java.awt.List;
import java.io.FileWriter;
import java.io.IOException;

public class JSONFactory {
	//private Cube cube;
	
	public void generateJSON() {
		Cube[] cubes = new Cube[2];
		cubes[0] = new Cube(1f);
		cubes[1] = new Cube(1f);
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				cubes[1].setPointColor(i, j, 7, new Color(200, 200, 200));
			}
		}
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				cubes[0].setPointColor(i, j, 0, new Color(100, 100, 100));
			}
		}
		
		String jsonString = "{\"FrameCount\":"+cubes.length + ",\"Frames\":[";
		for (int i = 0; i < cubes.length; i++) {
			jsonString += cubes[i].getFrameString();
			if(i != cubes.length-1) jsonString += ",";
		}
		jsonString += "]}";
		
		
		try (FileWriter file = new FileWriter("/Users/Sonrisa/Documents/file1.json")) {
			file.write(jsonString.toString());
			System.out.println("Successfully Copied JSON Object to File...");
			System.out.println("\nJSON Object: " + jsonString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}

