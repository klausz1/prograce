package default_class;

public class Cube {
	private static int CUBE_EDGE = 8;
	private static int NUMBER_OF_POINTS = CUBE_EDGE*CUBE_EDGE*CUBE_EDGE;
	
	private Point[] points = new Point[NUMBER_OF_POINTS];
	private Color defaultColor = new Color(0, 0, 0);
	private float time;
	
	private static int CUBE_WIDTH = 8;
	private static int CUBE_HEIGHT = 8;
	private static int CUBE_DEPTH = 8;
	
	public Cube(float time) {
		for (int i = 0; i < NUMBER_OF_POINTS; i++) {
			int x = getXfromIndex(i);
			int y = getYFromIndex(i);
			int z = getZFromIndex(i);
			points[i] = new Point(x, y, z, defaultColor);
			this.time = time;
		}
	}
	
	public void setPointColor(int x, int y, int z, Color color) {
		for (int i = 0; i < points.length; i++) {
			if(z == getZFromIndex(i) && y==getYFromIndex(i) && x==getXfromIndex(i)) {
				points[i].setColor(color);
			}
		}
	}
	
	private int getIndexFromCoordinate(int x, int y, int z) {
		//if(i/9 == z && i == (i%9)/3 && i== (i%9)%3)
		return z*CUBE_EDGE*CUBE_EDGE + y * CUBE_EDGE + x;
	}
	
	private int getXfromIndex(int index) {
		//int x = (i%9)%3;
		return (index%(CUBE_WIDTH*CUBE_HEIGHT)%CUBE_HEIGHT);
	}
	
	private int getYFromIndex(int index) {
		//int y = (i%9)/3;
		return (index%(CUBE_WIDTH*CUBE_HEIGHT))/CUBE_DEPTH;
	}
	
	private int getZFromIndex(int index) {
		//int z = i/9;
		return index/(CUBE_WIDTH*CUBE_HEIGHT);
	}
	
	public String getFrameString() {
		String frameString = "{\"SaveBlock\":[";
		for (int i = 0; i < points.length; i++) {
			frameString += ""+ points[i].getColorCode();
			if(i != points.length-1) frameString += ",";
		};
		frameString += "],\"time\":" + time +"}";
		return frameString;
	}
}
