package Valami.ProgRace1;

import default_class.Cube;
import default_class.JSONFactory;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        JSONFactory jFactory = new JSONFactory();
        jFactory.generateJSON();
    }
}
